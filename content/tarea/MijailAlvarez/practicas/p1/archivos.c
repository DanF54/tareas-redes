#include<stdio.h>
#include"lista.h"
#include<string.h>
#include<stdlib.h>

/* Imprime menu grafico
 */
void menu() {
  printf("\n\n\n1. Insertar archivo \n");
  printf("2. Leer archivo\n");
  printf("3. Eliminar archivo\n");
  printf("4. Imprimir archivos\n");
  printf("Presione Ctrl-c para salir del programa\n-->");
}

/* Funcion que devuelve la linea leida de la entrada estandar, sin el salto de
   linea(\n). (Solo los 256 caracteres). Ocupamos strchr, para quitar newline.
   This returns a pointer to the first occurrence of the character c in
   the string str, or NULL if the character is not found.
*/
char* regresaLinea() {
  char *linea;
  linea = malloc(256);
  fgets(linea, 256, stdin);
  char *saltoLinea = strchr(linea, '\n');
  if(saltoLinea != NULL) *saltoLinea = '\0';
  return linea;
}

/* Funcion que lee un numero(linea) del stdin y lo convierte a int, convierte
   cualquier cadena a su equivalente a entero, con la funcion atoi.
*/
int regresaNumero() {
  int numero;
  char *lineaNum;
  lineaNum = malloc(128);
  fgets(lineaNum, 128, stdin);
  return numero = atoi(lineaNum);
}

/* Inserta un archivo a nuestra lista de archivos(archivos), lo que hace es:
   tomar el "absolute path" del archivo que deseemos y ese path lo metera a
   archivos como un elemento de nuestra lista. Leemos el path de la entrada 
   estandar y lo guardamos.
*/
void insertarArchivo(lista *archivos) {
  printf("\n\n---Escriba el absolute path del archivo---\n"
	 "ó nombre del archivo si esta en la misma carpeta que este programa\n\n"
	 "-->");
  char *path = regresaLinea();
  agrega_elemento(archivos, path);
}

/* Leer archivo, lee un archivo(dado su respectivo absoluth path) e imprime
   el contenido del archivo en la terminal. Vamos a leer el achivo linea por
   linea, suponemos que cada linea tiene como maximo 1000 caracteres. 
   Con ayuda de la variable totalRead, eliminaremos el salto de linea \n que
   se imprime en la terminal, ya que fgets, obtiene el caracter \n de cada 
   linea.
*/
void leerArchivo(lista *listita) {
  int indice;
  printf("\n--Por favor escriba el indice del archivo que desee leer.--\n->");
  indice = regresaNumero();
  if(indice < listita->longitud
     && listita->longitud >= 0) {
    FILE *fpointer;
    char buffer[1000];
    int totalRead = 0;
    fpointer = fopen(obten_elemento(listita, indice), "r");
    if(fpointer == NULL) {
      printf("\nNo es posible encontrar este archivo, camino desactualizado"
	     " ó no valido\n");
    } else {
      while(fgets(buffer, 1000, fpointer) != NULL) {
	totalRead = strlen(buffer);
	buffer[totalRead - 1] = (buffer[totalRead - 1] == '\n')? '\0' :
	  buffer[totalRead - 1];
	printf("%s\n", buffer);
      }
      fclose(fpointer);
    }
  } else
    printf("\nPor favor escriba un indice valido o verifique que la lista"
	   " tenga elementos.\n");
}

/* Eliminar archivo de la lista.
   El usuario debe ingresar el indice de la lista a borrar.
   Se verifica si es un entero y si esta en el rango de indices de la lista.
*/
void eliminarArchivo(lista *listita) {
  printf("\n--Por favor escriba el indice del archivo que desee eliminar.--\n->");
  int indice = regresaNumero();
  if(indice < listita->longitud
     && listita->longitud >= 0) {
    printf("\nEl archivo %s fue borrado exitosamente\n",
	   (char*)obten_elemento(listita, indice));
    elimina_elemento(listita, indice);
  } else
    printf("\nPor favor escriba un indice valido o verifique que la lista"
	   " tenga elementos.\n");
}

/* Imprimir archivos, funcion la cual itera sobre la lista, imprimiendo los 
   elementos de esta, iniciando con su indice, para que el usuario pueda utilizar
   la funcion eliminarArchivo de una mejor manera.
*/
void imprimirArchivos(lista *listita) {
  if(listita->longitud == 0)
    printf("\n-----Lista vacia-----\n");
  else {
    printf("\n-----Los archivos guardados son:-----\n");
    int n = listita->longitud;
    for(int i = 0; i < n; i++) {
      printf("[%d]. %s\n", i, (char*)obten_elemento(listita, i));
    }
  }
}

/* Funcionamiento del menu  */
void menuFuncionamiento(lista *listita) {
  int opcion;
  while(1) {
    menu();
    opcion = regresaNumero();
      switch(opcion) {
      case 1:
	insertarArchivo(listita);
	break;
      case 2:
	leerArchivo(listita);
	break;
      case 3:
	eliminarArchivo(listita);
	break;
      case 4:
	imprimirArchivos(listita);
	break;
      default:
	printf("\nPor favor ingresa una opcion(numero) valida.\n");
	break;
    }
  }
}

/* Funcion main */
int main(){
  lista lista = {cabeza:0, longitud:0};
  menuFuncionamiento(&lista);
}
