# Práctica 3

## Compilación
```sh
$ make
```
## Imagenes de Ejecución

### file.c

Programa file.c con diferentes archivos
![N|Solid](img/file.png)

### strings

Archivo ejemplo (en carpeta util)
![N|Solid](img/strings1.png)

Programa en ejecución con archivo ejemplo
![N|Solid](img/strings2.png)


### hashes
Programa en ejecución con hash md5
![N|Solid](img/md5.png)

Programa en ejecución con hash sha1
![N|Solid](img/sha1.png)

Programa en ejecución con hash sha256
![N|Solid](img/sha256.png)


### filtros

Se encuentran en el archivo filtros.txt

1. Mostrar paquetes que tengan por dirección MAC origen 60:a4:4c:89:3c:a4 o que tengan por dirección MAC destino fc:fb:fb:01:fa:21.
	- tcpdump "ether src 60:a4:4c:89:3c:a4" and "ether dst fc:fb:fb:01:fa:21"
2. Mostrar paqutes cuya dirección MAC ya sea origen o destino comience con 88:53:95.
	- tcpdump "ether[0:4]=0x885395"
3. Mostrar paquetes cuyo ethertype sea IPv6.
	- tcpdump ip6
4. Mostrar paqutes que tengan por ethertype a ARP y cuya dirección MAC destino sea ff:ff:ff:ff:ff:ff.
	- tcpdump arp and "ether dst ff:ff:ff:ff:ff:ff"
5. Mostrar paquetes cuya dirección IP empieza con 172.
	- tcpdump host 172
