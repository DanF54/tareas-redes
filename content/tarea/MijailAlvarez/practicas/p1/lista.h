#ifndef __LISTA_H
#define __LISTA_H
/* Practica 1- Redes de Computadoras 2019-2 FC
   Lista Enlazada en C, con estructuras y apuntadores
   Alvarez Cerrillo Mijail Aron
   310020590
*/

/* A struct in the C programming language (and many derivatives) is a composite
   data type (or record) declaration that defines a physically grouped list of 
   variables to be placed under one name in a block of memory, allowing the
   different variables to be accessed via a single pointer, or the struct 
   declared name which returns the same address. The struct data type can 
   contain many other complex and simple data types in an association, so it is
   a natural type for organizing mixed-data-type records such as lists of 
   hard-drive directory entries (file length, name, extension, physical 
   (cylinder, disk, head indexes) address, etc.), or other mixed-type records
   (patient names, address, telephone... insurance codes, balance, etc.).
   Wikipedia
*/

/* A typedef, in spite of the name, does not define a new type;
   it merely creates a new name for an existing type.
*/

/* Estructura para representar a los nodos los cuales pueden ser
   de cualquier tipo ya que tenemos apuntadores genericos con void. Un nodo
   contiene un elemento y una referencia a su siguiente elemento. La funcion
   es usada para crear cada nodo de la lista. Le ponemos el alias(typef): nodo,
   de esta manera al referirnos a ella solo con poner "nodo" bastaria, en lugar 
   de poner "struct nodo".
*/
typedef struct nodo {
  void *elemento;
  struct nodo *siguiente;
} nodo;

/* Estructura para representar a las listas, una lista de este tipo contiene
   dos caracteristicas un nodo cabeza y la longitud de dicha lista.
   Define cabeza como apuntador de la lista.  Le ponemos el alias(typef): lista,
   de esta manera al referirnos a ella solo con poner "lista" bastaria, en lugar
   de poner "struct lista".
*/
typedef struct lista {
  struct nodo *cabeza;
  int longitud;
} lista;

/* 
   Funcion para crear un nodo(inicializar), declaramos un nodo temporal, le 
   asignamos memoria (malloc) y ponemos su referencia a siguiente.
*/
nodo *crearNodo();

/* Función que agrega un elemento al final de la lista, esta lista a la cual se
   le añadira un nodo con el elemento la llamaremos "listAModificar" accederemos
   a ella por medio de apuntadores. Creamos dos nodos uno donde ira el elemento
   agregado(temporal) y otro para recorrer la lista(apuntador). Si la lista a 
   modificar es vacia entonces el temporal lo hacemos su cabeza e incrementamos
   la longitud en uno. Si no es vacia el nodo apuntador lo ponemos en la cabeza
   y recorremos la lista hasta que un nodo tenga como siguiente a NULL(final de
   la lista), entonces enlazamos el nodo temporal a la lista e incrementamos la 
   longitud de la lista.
*/
void agrega_elemento(lista *listAModificar, void *elementoAgregado);

/*
  Permite obtener el n-elemento de la lista, empezando desde 0.
  Regresa:
  El elemento número n de la lista.
  0 si n está fuera del rango de la lista.
*/
void * obten_elemento(lista * listita, int n);

/* Elimina el n-elemento de la lista, empezando desde 0.
   Regresa:El elemento eliminado.
   0 si n está fuera del rango de la lista.
   Recordar liberar la memoria asignada dinámicamente que ya no vaya a utilizarse.
*/
void * elimina_elemento(lista *listita, int n);


/* 
   Aplica la función f a todos los elementos de la lista.
*/
void aplica_funcion(struct lista *lista, void (*f)(void *));

#endif
