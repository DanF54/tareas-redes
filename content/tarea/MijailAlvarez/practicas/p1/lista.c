/* Practica 1- Redes de Computadoras 2019-2 FC
   Lista Enlazada en C, con estructuras y apuntadores
   Alvarez Cerrillo Mijail Aron
   310020590
*/
#include <stddef.h> // NULL definition
#include <stdlib.h> // malloc, calloc & free
#include "lista.h"  // Structures 

/* 
   Funcion para crear un nodo(inicializar), declaramos un nodo temporal, le 
   asignamos memoria (malloc) y ponemos su referencia a siguiente.
*/
nodo *crearNodo() {
  nodo *temporal = NULL;
  temporal = malloc(sizeof(nodo));
  temporal->siguiente = NULL;
  return temporal;
}

/* Función que agrega un elemento al final de la lista, esta lista a la cual se
   le añadira un nodo con el elemento la llamaremos "listAModificar" accederemos
   a ella por medio de apuntadores. Creamos dos nodos uno donde ira el elemento
   agregado(temporal) y otro para recorrer la lista(apuntador). Si la lista a 
   modificar es vacia entonces el temporal lo hacemos su cabeza.
   Si no es vacia el nodo apuntador lo ponemos en la cabeza
   y recorremos la lista hasta que un nodo tenga como siguiente a NULL(final de
   la lista), entonces enlazamos el nodo temporal a la lista. Incrementamos la 
   longitud de la lista.
*/
void agrega_elemento(lista *listAModificar, void *elementoAgregado) {
  nodo *temporal, *apuntador;
  temporal = crearNodo();
  temporal->elemento = elementoAgregado;
  if(listAModificar->cabeza == NULL) {
    listAModificar->cabeza = temporal;
  } else {
    apuntador = listAModificar->cabeza;
    while(apuntador->siguiente != NULL)
      apuntador = apuntador->siguiente;
    apuntador->siguiente = temporal;
  }
  listAModificar->longitud ++;
}

/*
  Permite obtener el n-elemento de la lista, empezando desde 0.
  Regresa:
  El elemento número n de la lista.
  0 si n está fuera del rango de la lista.
*/
void * obten_elemento(lista *listita, int n) {
  if(n > listita->longitud - 1)
    return 0;
  else {
    nodo *actual = crearNodo();
    actual = listita->cabeza;
    for (int contador = 0; contador < n; contador++)
      actual = actual->siguiente;
    return actual->elemento;
  }
}

/* Elimina el n-elemento de la lista, empezando desde 0.
   Regresa:El elemento eliminado.
   0 si n está fuera del rango de la lista.
   Recordar liberar la memoria asignada dinámicamente que ya no vaya a utilizarse.
*/
void * elimina_elemento(lista *listita, int n) {
  if(n > listita->longitud - 1)
    return 0;
  else {
    void *elementoregresar;
    // Lista de un solo elemento
    if(listita-> longitud == 1) {
      elementoregresar = listita->cabeza->elemento;
      free(listita->cabeza);
      // El nodo a borrar es la cabeza
    } else if(n == 0) {
      nodo *temporal = crearNodo();
      elementoregresar = listita->cabeza->elemento;
      temporal = listita->cabeza->siguiente;
      free(listita->cabeza);
      listita->cabeza = temporal;
      // El nodo a borrar el la cola
    } else if(n == listita->longitud -1) {
      nodo *anteriorCola = crearNodo(), *colaBorrada;
      anteriorCola = listita->cabeza;
      for(int i = 0; i < n - 1; i++)
	anteriorCola = anteriorCola->siguiente;
      colaBorrada = anteriorCola->siguiente;
      anteriorCola->siguiente = NULL;
      elementoregresar = colaBorrada->elemento;
      free(colaBorrada);
      // El nodo a borrar esta entre la cabeza y la cola
    } else {
      nodo *anteriorDelBorrado, *borrado;
      anteriorDelBorrado = listita->cabeza;
      for(int i = 0; i < n - 1; i++)
	anteriorDelBorrado = anteriorDelBorrado->siguiente;
      borrado = anteriorDelBorrado->siguiente;
      elementoregresar = borrado->elemento;
      anteriorDelBorrado->siguiente = borrado->siguiente;
      free(borrado);
    }
    listita->longitud--;
    return elementoregresar;
  }
}

/* 
   Aplica la función f a todos los elementos de la lista.
   Void functions are created and used just like value-returning functions 
   except they do not return a value after the function executes. In lieu 
   of a data type, void functions use the keyword "void." A void function
   performs a task, and then control returns back to the caller--but, it
   does not return a value.
*/
void aplica_funcion(lista *listita, void (*f)(void *)){
  nodo *recorre;
  recorre = listita->cabeza;
  for (int i = 0; i < listita->longitud; i++) {
    f(recorre->elemento);
    recorre = recorre->siguiente;
  }
}
