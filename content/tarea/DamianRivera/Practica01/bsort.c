#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <limits.h>


/* Función para imprimir un arreglo de enteros*/
void printArray(int arr[], int size)
{
    int i;
    printf("%s ", "Arreglo: ");
    for (i=0; i < size; i++)
    {
    	printf(" %d ",arr[i]);

    }
    printf("\n");
}

/* Función para imprimir un arreglo de apuntadores */
void printArrayPuntero(int *arr[], int size)
{
    int i;
    printf("%s ", "Ordenado: ");
    for (i=0; i < size; i++)
    {
      printf(" %d ", *(arr[i]));
    }
    printf("\n");
}

/*
* Función que implementa el ordenamiento BubbleSort
*/
void bubbleSort(int arr[],int *arrPuntero[], int n)
{
  int i, j;
 for (i = 0; i < n-1; i++)
 {
     for (j = 0; j < n-i-1; j++)
     {
        
        //Realizamos el swap dentro de las referencias.
        if (*(arrPuntero[j]) > *(arrPuntero[j+1]))
        {
          int *punterotemp = arrPuntero[j];
          arrPuntero[j] = arrPuntero[j+1];
          arrPuntero[j+1] = punterotemp;
        }
     }

 }

}


int main(int argc, char const *argv[])
{
  int arr[argc-1];
  int *arrPuntero[argc-1];
  printf("***** Ordenamiento Burbuja ***** \n");
  int i = 0;
  for (i = 0; i < argc-1; ++i)
  { 
    arr[i] = atoi(argv[i+1]);
    arrPuntero[i] = &arr[i];
  }
 int n = argc-1;
 printArray(arr, n);
 bubbleSort(arr, arrPuntero, n);
 printArrayPuntero(arrPuntero, n);
  return 0;
}
