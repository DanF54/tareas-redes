#include<stdio.h>
#include<string.h>
#include<stdlib.h>

/*
* Implementación de una lista mediante estructuras y apuntadores.
* Autor: Damián Rivera González.
*/

/* 
* Estructura para una nodo elemento de la lista
* Cada Nodo tiene un elemento y un nodo siguiente.
*/
typedef struct nodo {
	void * elemento;
	struct nodo * siguiente;
}Nodo;

/*
* Estructura para una lista
* Tiene un elemento cabeza de tipo estructura nodo y
* una longitud (número de elementos nodo en la lista)
*/
typedef struct lista{
	struct nodo* cabeza;
	int longitud;
}Lista;

 /**
 * Función que agrega un elemento al final de la lista.
 * recibe el apuntador de una estructura 'lista' y 
 * un apuntador a un 'elemento' genérico.
 * El elemento que recibe lo almacena en una estructura Nodo y 
 * lo agrega al final de la lista.
 * @param  lista: apuntador a la lista de elementos genericos
 * @param  elemento: apuntador al elemento que se agregará a la lista.
 * @retval None
 */
void agrega_elemento(struct lista * lista, void * elemento)
{
	Nodo * nuevo = malloc(sizeof(nuevo));
	nuevo -> elemento = elemento;

	//Si es una lista vacía agregamos el nuevo nodo como referencia de la cabeza.
	if (lista -> longitud == 0 && lista -> cabeza == 0)
	{	
		lista -> cabeza = nuevo;
	}
	else
	{	
		//Creamos un nodo para recorrer la lista.
		Nodo * ultimo = malloc(sizeof(nuevo));
		ultimo = lista -> cabeza;

		//Mientras un nodo tenga siguiente avanzamos.
		while(ultimo -> siguiente){
			ultimo = ultimo -> siguiente;//actualizamos el nodo que itera.
		}
		//asignamos la referencia 'siguiente' del nodo final como el nuevo nodo
		ultimo -> siguiente = nuevo;
	}
	lista -> longitud = lista -> longitud+1;//aumentamos la lontigud de la lista
	printf("Se agregó un nuevo elemento.\n");
}

 /*
 * Función que obtiene el n-ésimo lugar de la lista.
 * Tiene como parametros un apuntador a una estructura 'lista' y
 * un entero índice del elemento a obtener.
 * @param  lista: apuntador a la lista de elementos genericos. 
 * @param  n: índice del elemento para obtener.
 * @retval un apuntador al n-ésimo elemento.
 */
void * obten_elemento(struct lista* lista, int n)
{
	//Valor que re gresa en caso de dar un índice no válido.
	int x = 0;
	int * p = &x; 

	void * elemento;//Variable para guardar el valor del elemento obtenido.
	//Si el índice no está dentro del rango de la lista.
	if(n >= lista-> longitud || n < 0 || lista -> cabeza == NULL){
		printf("Índice fuera de rango\n");
		return p;//Regresamos 0
	}
	else if(lista -> cabeza)
	{
		//Si queremos el primer elemento.
		if (n == 0)
		{
			elemento = lista -> cabeza -> elemento;//Regresamos la cabeza
		}
		else
		{
			//Estructura nodo para iterar la lista.
			Nodo * nodo =  lista -> cabeza;
			//Recorremos el nodo iterador n veces.
			while(n > 0){
				nodo = nodo -> siguiente;
				n--;
			}
			//Regresamos el elemento del Nodo n.
			elemento = nodo -> elemento;
		}
		return elemento;
	}
	return p;//Regresamos 0
}

/*
 * Función que dada la referencia a una estructura de tipo 'Lista'
 * y un índice 'n', se elimina el elemento de la lista con índice n.
 * Regresa el apuntador al elemento eliminado de la lista.
 * @param  lista: apuntador a la lista de elementos genericos 
 * @param  n: índice del elemento a eliminar 
 * @retval - apuntador a un elemento generico.
 */
void * elimina_elemento(struct lista* lista, int n)
{
	//variable para regresar en caso de no indicar un índice válido.
	int x = 0;
	int *p = &x;

	//Verificamos que el índice desea se encuentre dentro del rango.
	if (n >= lista-> longitud || n < 0 || lista -> cabeza == NULL)
	{
		printf("Índice fuera de rango\n");
		return p;
	}
	//Si la lista no es vacia.
	else if (lista -> cabeza)
	{
		//referenecia para el elemento a regresar.
		void * elemento;
		//Si la lista está vacía
		if (n == 0)
		{
			//imprimimos el elemento de la cabeza
			elemento = lista -> cabeza -> elemento;
			//cambiamos la referencia de la cabeza por su siguiente.
			lista -> cabeza = lista -> cabeza -> siguiente;
		}
		else
		{	
			//referencia para conocer el nodo a eliminar.
			Nodo * aux1 = lista -> cabeza -> siguiente; 
			//referencia del nodo que se actualizará su elemento siguiente quitando el n-ésimo elemento.
			Nodo * aux2 = lista -> cabeza;
			//Iteramos n veces sobre los nodos de la lista.
			while(n > 1){
				//Vamos actualizando las referencias de los nodos auxiliares.
				aux1 = aux1 -> siguiente;
				aux2 = aux2 -> siguiente;
				n--;
			}//Una vez que iteramos n veces.
			//Guardamos el contenido del nodo a eliminar.
			elemento = aux1 -> elemento;
			//actualizamos las referencias en la lista para eliminar el nodo n.
			aux2 -> siguiente = aux1 -> siguiente;
		}
		//Decrementamos la longitud de la lista.
		lista -> longitud = lista -> longitud-1;
		printf("Se eliminó un elemento.\n");
		return elemento;
	}
	return p;
}

/*
 * Función que aplica la función que se pasa como parametro
 * a cada elemento dentro de la estructura lista que se pasa como parametro.
 * @param  lista: apuntador a la lista de elementos genericos 
 * @param  (*f: apuntador a la función que se aplicará a los elementos.
 */
void aplica_funcion(struct lista* lista, void (*f)(void *))
{
	if (lista->longitud != 0) {
		//Nodo inicial para iterar la lista.
		Nodo * nodo = lista -> cabeza;
		//mientras haya un nodo que recorrer
		while(nodo -> siguiente){
			//aplicamos la función al elemento dentro del nodo actual
			f(nodo -> elemento);
			//Avanzamos al siguiente nodo.
			nodo = nodo -> siguiente;
		}
		//Aplicamos la función al último nodo de la lista.
		f(nodo -> elemento);	/* code */
	}
}