#include<stdio.h>
#include<dirent.h>

 /*
 * Función que imprime como cadena el contenido
 * que se tiene dentro de 'elemento'.
 * Recibe el apuntador a un elemento generico.
 * @param  elemento: el apuntador del elemento generico que imprimirá.
 * @retval None
 */
void imprime(void * elemento) {
    //Memoria para guardar como cadena  el contenido del apuntador generico
	char * contenido = malloc(sizeof(char));
    contenido = elemento;//asignamos el contenido del apuntador.
	printf("%s ", contenido);
}

/**
 * Función que dado un apuntador a un directorio 
 * lee sus archivos contenidos.
 * @param  directorio: apuntador generico. 
 * @retval None
 */
void leerDirectorio(void * directorio){
    struct dirent *dir;//apuntador para el directorio a entrar.
    DIR *folder = opendir(directorio);//apuntador de tipo directorio.
    
    int archivos = 0;//índice del archivo.
    //Si no se puede cargar como directorio el elemento.
    //mandamos error.
    if(folder == NULL)
    {
        perror("No se pudo leer el directorio.");
    }
    //En otro caso imprimimos el contenido.
    printf("Contenido del directorio:\n");
    while((dir = readdir(folder)))//Leemos los elementos dentro del apuntador dir.
    {
        archivos++;
        printf("Archivo %d: %s\n", archivos ,dir -> d_name );//Imprimimos los elementos.
    }
    closedir(folder);
}
/**
 * Función que dado un apuntador a un archivo, 
 * lee su contenido línea por línea. 
 * @param  archivo: 
 * @retval None
 */
void leerArchivo(void * archivo){
    char ch;
    FILE *fp;
    //abrimos el archivo en modo lectura.
    fp = fopen(archivo, "r");
    //Si no existe el archivo mandamos un error.
    if (fp == NULL)
    {
        perror("El archivo no existe.\n");
    }
    //SI el archivo existe 
    else
    {
        printf("Contenido del archivo :\n");
        //Imprimimos el contenido del archivo mientras no lleguemos al final del archivo.
        while((ch = fgetc(fp)) != EOF)
        {
            printf("%c", ch);
        }
    }
    fclose(fp);//Cerramos el flujo de información.
}
