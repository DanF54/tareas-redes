#include <stdlib.h>
#include <stdio.h>
#include <openssl/sha.h>
#include <stddef.h>
#include <errno.h>
#include <string.h>

void sha_hash_string (char hash[SHA_DIGEST_LENGTH], char outputBuffer[65])
{
    for(int i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        sprintf(outputBuffer + (i * 2), "%02x", (unsigned char)hash[i]);
    }
    outputBuffer[64] = 0;
}

void sha(char *string, char outputBuffer[65])
{
    unsigned char  hash[SHA_DIGEST_LENGTH];
    int len;
    SHA_CTX sha;
    SHA1_Init(&sha);
    len=strlen(string);
    SHA1_Update(&sha, string,len);
    SHA1_Final(hash, &sha);
    int i = 0;
    for(i = 0; i < SHA_DIGEST_LENGTH; i++)
    {
        sprintf(outputBuffer + (i * 2), "%02x", (unsigned char)hash[i]);
    }
    outputBuffer[64] = 0;
}

int sha_file(char *path, char outputBuffer[65])
{
    FILE *fich = fopen(path, "rb");
    if(!fich) return -534;
    unsigned char hash[SHA_DIGEST_LENGTH];
    SHA_CTX sha;
    SHA1_Init(&sha);
    const int bufSize = 32768;
    unsigned char *buffer = malloc(bufSize);
    int bytesRead = 0;
    if(!buffer) return ENOMEM;
    while((bytesRead = fread(buffer, 1, bufSize, fich)))
    {
        SHA1_Update(&sha, buffer, bytesRead);
    }
    SHA1_Final(hash, &sha);
    sha_hash_string(hash, outputBuffer);
    fclose(fich);
    free(buffer);
    return 0;
}
